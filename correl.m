%hypothesis testing and correlation 
numsim=1000;
samplesize=50;
results=zeros(1,numsim);
num=1
data=randn(samplesize,2);
size(data)
corr(data(:,1),data(:,2))
results(num)=corr(data(:,1),data(:,2));
results(1:10)
for num=1:numsim
data=randn(samplesize,2);
results(num)=corr(data(:,1),data(:,2));
end
results(1:10)
%visualize the results
figure;hold on;
hist(results,100);
ax=axis
mx=max(abs(ax(1:2)));
axis([-mx mx ax(3:4)]);
xlabel('correlation value');
ylabel('Frequency');
val=prctile(abs(results),95);
%visualize this on the figure
ax=axis;
h1=plot([val val],ax(3:4),'r-');
h1=plot([val val],ax(3:4),'r-');
h2=plot(-[val val],ax(3:4),'r-');
legend(h1,'central 95%');
title(sprintf('+/- %.4f',val));
rand(1,50);
figure;
hist(rand(1,1000),100);
x=rand(1,100);
y=rand(1,100);
figure;
scatter(x,y,'r.');
actcrl=corr(x(:),y(:));
results=zeros(1,10000);
num=1;
ix=ceil(length(x)*rand(1,length(x)));
results=zeros(1,10000);
for num=1:10000
ix=ceil(length(x)*rand(1,length(1,x)));
%compute correlations using these data point
results(num)=corr(x(ix)',y(ix)');
end
for num=1:10000
ix=ceil(length(x)*rand(1,length(x)));
%compute correlations using these data point
results(num)=corr(x(ix)',y(ix)');
end
figure;
hist(results);
figure;hist(results,100);
scatter(x,y,'ro');
xlabel('x');
ylabel('y');
randperm(5)

results=zeros(1,10000);
for num=1:10000
ix=randperm(length(x));
ix2=randperm(length(x));
 results(num)=corr(x(ix)',y(ix2)');
end
%visualization
figure;hold on;
hist(results,100);