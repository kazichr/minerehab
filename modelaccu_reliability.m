%model accuracy and reliability 
data=rand(1,100);
n=length(data);
figure;
hist(data);
p=1;
bootix=ceil(n*rand(1,n));% takes integer numbers a indices for matrix
union([],bootix);
data(bootix);%gives bootstraps sample
numboots=1000;%how many bootstraps to perform
for p=1:numboots
    bootix=ceil(n*rand(1,n));%indices to use for bootstrap sample
    data(bootix);%gives bootstraps sample
end
trainix=setdiff(1:n,p);%indices for training
testix=p; %indices to use for testing
data(trainix);%99 numbers

% in leave-one-out cross validation,we leave out one data point 
%train on the remaining data points and then test on the left out data
%point. this process is repeated for each data point.
for p=1:n
trainix=setdiff(1:n,p);
testix=p;
data(trainix);
data(testix);
end