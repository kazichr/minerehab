%Fit a parametric nonlinear model
x=0:.05:03;
y=5*x.^3.5+5*randn(size(x));
%define optimization options
options=optimset('Display','iter','FunValCheck','on',...'MaxFunEvals',Inf,'MaxIter',Inf,...,'Tolfun',1e-6,'TolX',1e-6);
%define bounds for the parameters
% a n
paramslb = [-Inf 0];%lower bound for parameters
paramsub=[ Inf Inf];%upper bound for parameters
params0=[1 1];
%anonymous function as there is @sign
modelfun = @(pp,xx) pp(1)*xx.^pp(2);
[params,resnorm,residual,exitflag,output]=lsqcurvefit(modelfun,params0,x,y,paramslb, paramsub,options);