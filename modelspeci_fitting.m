%fit a linearized regression model
x=-20:20;
size(x)
y=1.5*x.^2-60*x+10+30*randn(size(x));%simulated data
size(y)
figure;scatter(x,y);
X=[x(:).^2 x(:) ones(length(x),1)];%regression matrix
size(X)
figure;
imagesc(X);
colormap(gray);
colorbar;
figure;plot(X);
w=inv(X'*X)*X'*y(:);%weights
modelfit=X*w;
size(modelfit);
figure; hold on;

scatter(x,y,'r');
squarederror=sum((y(:)-modelfit).^2);
ax=axis;
xx=linspace(ax(1),ax(2),100);
yy = [xx(:).^2 xx(:) ones(length(xx),1)]*w;
size(yy);
plot(xx,yy,'k-');

xlabel('x');
ylabel('y');
title(sprintf('squared error %f',squarederror));