%Bootstrap a simple linear model
n=100;
x=randn(1,n);
y=10*x+2+4*randn(1,n);
figure;scatter(x,y);
numboots=1000;
xvals=-3:0.5:3;%x-value to evaluate the model at
%perform the bootstrap
modelfit=zeros(numboots,length(xvals));
params=zeros(numboots,2);
for boot=1:numboots
    %prepare data indices to take random bootstrap
    ix=ceil(n*rand(1,n));
    %construct regressor matrix
    X=[x(ix)' ones(n,1)];
    %size(X);
    h=inv(X'*X)*X'*y(ix)';%estimate parameters
    %evaluate the model
    modelfit(boot,:)=xvals*h(1)+h(2);
    params(boot,:)=h;%record the parameters
end
%Visualize
figure;
hold on;
h1=scatter(x,y,'k.');
modelfitp=prctile(modelfit,[2.5 50 97.5],1);
h2=patch([xvals fliplr(xvals)],[modelfitp(1,:) fliplr(modelfitp(3,:))],[1 .7 .7])
h3=plot(xvals,modelfitp(2,:),'r-','Linewidth',2);% plot a line for the mediun
wistack(h1,'top');%make sure data points are visible by bringing them to the top